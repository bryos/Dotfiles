#!/usr/bin/env bash
[ -e "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc" ] \
    && . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc"
[ -e "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bash_aliases" ] \
    && . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bash_aliases"
