#!/bin/sh
#
# Description:
# Create symbolic links for all config files to the appropriate places.

: "${SUDO:=sudo -A}" \
  "${MY_PROJECTS_DIR:=${HOME}/.local/my-projects}" \
  "${DOTFILE_DIR:=${MY_PROJECTS_DIR}/gitlab/dotfiles}"

readonly \
    DOTFILE_DIR \
    MY_PROJECTS_DIR \
    SUDO

backup() { # Backup a file or directory
    # usage: backup FILE [sudo -A | doas]
    [ -f "$1" ] || [ -d "$1" ] && $2 mv "$1" "$1".bak
    return
}

symlink() { # symlink dotfile file/directory to appropriate place
    for target in "$@"; do
        backup "${HOME}/${target}"

        ln -vs "${DOTFILE_DIR}/${target}" "${HOME}/${target}"
    done
    return
}

rmlinks() { # remove the links created by symlink()
    # TODO: remove symlinks in root
    for target in "$@"; do
        target_path="${HOME}/${target}"
        [ -L "${target_path}" ] && unlink "${target_path}"

        [ -e "${target_path}".bak ] \
            && mv "${target_path}".bak "${target_path}"
    done
    unset target_path
    return
}

root_links() { # create the system level symlinks
    [ -e /etc/doas.conf ] || SUDO='sudo -A'

    for target in "${DOTFILE_DIR}"/root/*; do
        link_name="$(echo "${target}" | sed "s|${DOTFILE_DIR}/root/||; s|!|/|g")"
		link_dir="$(dirname "${link_name}")"

        [ -L "${link_name}" ] || backup "${link_name}" "${SUDO}"
		[ -d "${link_dir}" ] || mkdir -pv "${link_dir}"

        ${SUDO} ln -vs "${target##*!}" "${link_name}"
    done
}

case "$1" in
    i|install)
        echo 'Install the files in ./root (y/N): '
        read -r install_root
        symlink .config .local/mybin .profile
        [ "${install_root}" = y ] && root_links
        ;;
    u|update) root_links ;;
    U|uninstall) rmlinks .config .local/mybin .profile ;;
    *) echo "Usage: $0 [un]install | update"
esac
