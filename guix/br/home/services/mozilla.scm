(define-module (br home services mozilla)
  #:use-module (gnu home services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu)
  ;; #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (nongnu packages mozilla)
  #:export (home-firefox-service-type
            home-firefox-configuration
            ))


;;; Commentary:
;;;
;;; This module contains firefox related services.
;;;
;;; Code:


;;;
;;; Firefox
;;;

(define-configuration home-firefox-configuration
  ;; Do not add firefox to the home profile
  ;; (package
  ;;  (package firefox)
  ;;  "The firefox package to use.")
  (profile
   (string "")
   "Path to the profile to customize, e.g.
@file{~/.mozilla/firefox/<profile>}. Where <profile> will look something like
@file{abcdefgh.default}.")
  (userchrome
   (alist '())
   "List of files that provides should end up in the profile directory
e.g. @file{user.js} and @file{chrome/userchrome.css}. Specify a @code{(KEY
. VALUE)}, where KEY location inside the PROFILE and VALUE a path that can be
supplied to @code{local-file}.")
  (moz-enable-wayland
   (boolean #f)
   "Environment variable that enables Wayland for Mozilla products.")
  (no-serialization))

;; (define (home-firefox-profile-service config)
;;   (list
;;    (home-firefox-configuration-package config)
;;    (home-firefox-configuration-userchrome config)))

(define (home-firefox-environment-variables-service config)
  (if (home-firefox-configuration-moz-enable-wayland config)
      '(("MOZ_ENABLE_WAYLAND" . "true"))
        '()))

(define (home-firefox-config-files-service config)
  (let ((userchrome (home-firefox-configuration-userchrome config))
        (profile (home-firefox-configuration-profile config)))
    (map (lambda (file)
           (list
            (in-vicinity profile
                         (car file))
            (local-file (cdr file))))
         userchrome
     )))

(define home-firefox-service-type
  (service-type (name 'home-firefox)
                (extensions
                 (list (service-extension
                       ;;  home-profile-service-type
                       ;;  home-firefox-profile-service)
                       ;; (service-extension
                        home-files-service-type
                        home-firefox-config-files-service)
                       (service-extension
                        home-environment-variables-service-type
                        home-firefox-environment-variables-service)
                       ))
                (default-value (home-firefox-configuration))
                (description "Install and configure @code{firefox}.")))
