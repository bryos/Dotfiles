(define-module (br home services utils)
  #:use-module (gnu)
  #:export (xdg-config-files-symlink-target))

(define* (xdg-config-files-symlink-target file #:key recursive?)
  ;; Create a list such that it can be used with
  ;; 'home-xdg-configuration-files-service-type'. If recursive is true
  ;; with a file the permission bits are kept. If recursive is true
  ;; with a directory then the entire directory will be added to the
  ;; store.
  (list file
        (local-file (string-append
                     (or (getenv "DOTFILE_DIR")
                         (string-append (getenv "HOME")
                                        "/my-projects/gitlab/dotfiles"))
                     "/.config/" file)
                    #:recursive? recursive?)))
