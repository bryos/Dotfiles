(define-module (my-scripts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system guile)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (gnu packages wget)
  )

(define (script? file stat)
  (letrec ((has-suffix? (lambda (args)
                          (cond ((null? args) #f)
                                ((string-suffix? (car args) file)
                                 #t)
                                (else (has-suffix? (cdr args)))))))
    (not (or (eq? 'directory (stat:type stat))
             (has-suffix? '(".org" ".scm"))))))

(define-public br-utils
  (package
    (name "br-utils")
    (version "0.1")
    (source (local-file "../../.guile/modules"
                        "br-utils"
                        #:recursive? #t))
    (build-system guile-build-system)
    (native-inputs (list guile-3.0-latest))
    (synopsis "Some utility guile functions for @code{my-scripts} package")
    (description "This module implements some functions regarding shell IO and using
@code{dmenu} like applications.")
    (home-page "https://bryanrinders.xyz")
    (license license:public-domain)))

(define-public my-scripts
  (package
    (name "my-scripts")
    (version "0.2")
    (source (local-file "../../." "my-scripts"
                        #:recursive? #t
                        #:select? script?))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("." "bin"))))
    (propagated-inputs (list
                        br-utils
                        curl
                        dash
                        ffmpeg
                        findutils
                        gawk
                        gnupg
                        grep
                        guile-3.0-latest
						guile-gnutls
						guile-json-4
                        mpv
                        rename
                        sed
                        wget
                        yt-dlp
                        ))
    (synopsis "My scripts; automating various tasks")
    (description "My scripts automate a variety of tasks such as mounting devices,
changing themes and more.")
    (home-page "https://bryanrinders.xyz")
    (license license:public-domain)
    ))

;; my-scripts
